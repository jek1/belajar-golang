package main

import "fmt"

func findMax(numbers []int, max int) (int, func() []int) {
    var res []int
    for _, e := range numbers {
        if e <= max {
            res = append(res, e)
        }
    }
    return len(res), func() []int {
        return res
    }
}

func main() {
    var max = 3
    var numbers = []int{2, 3, 0, 4, 3, 2, 0, 4, 2, 0, 3}
    var howMany, getNumbers = findMax(numbers, max)
    var theNumbers = getNumbers()

    fmt.Println("numbers\t:", numbers)
    fmt.Printf("find \t: %d\n\n", max)

    fmt.Println("found \t:", howMany)    // 9
    fmt.Println("value \t:", theNumbers)    // [2 3 0 3  2 0 2 0 3]

    numbers = []int{2,1,0,2,3,4,5,6,7,8,3,3,3,3,3}
    _, getNumbers = findMax(numbers, max)
    theNumbers = getNumbers()
    fmt.Println("value \t:", theNumbers)    // [2 3 0 3  2 0 2 0 3]

}
