package main

import "flag"
import "fmt"

func main() {
	var name string
	var age int64


	flag.StringVar(&name, "name", "anonymous", "type your name")
	flag.Int64Var(&age, "age", 25, "type your age")

	flag.Parse()
	fmt.Printf("name\t: %s\n", name)
	fmt.Printf("age\t: %d\n", age)
}
