package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-oci8"
)

func openDB() *sql.DB {
	// connString := "system/oracle@localhost:1521/xe"
	connString := getDSN()
	db, err := sql.Open("oci8", connString)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func getDSN() string {
	var dsn string
	if len(os.Args) > 1 {
		dsn = os.Args[1]
		if dsn != "" {
			return dsn
		}
	}
	dsn = os.Getenv("GO_OCI8_CONNECT_STRING")
	if dsn != "" {
		return dsn
	}

	fmt.Fprintln(os.Stderr, `Please specifiy connection parameter in:
	GO_OCI8_CONNECT_STRING environment variable, or as the first argument!
	(The format is user/pass@host:port/servicename)`)
	os.Exit(100)
	return ""
}
