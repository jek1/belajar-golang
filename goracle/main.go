package main

import "fmt"

type tJob struct {
	JobID     string
	JobTitle  string
	MinSalary int64
	MaxSalary int64
}

func main() {
	db := openDB()
	defer db.Close()

	sql := "SELECT JOB_ID, JOB_TITLE, MIN_SALARY, MAX_SALARY FROM HR.JOBS"
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println(err)
		return
	}

	var listJob []tJob

	defer rows.Close()
	for rows.Next() {
		var JobID string
		var JobTitle string
		var MinSalary int64
		var MaxSalary int64
		rows.Scan(&JobID, &JobTitle, &MinSalary, &MaxSalary)

		var one = tJob{JobID, JobTitle, MinSalary, MaxSalary}
		listJob = append(listJob, one)
	}

	fmt.Println("listJob: ", listJob)
}
