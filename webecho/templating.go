package main

import (
	"io"
	"text/template"

	"github.com/labstack/echo"
)

type Renderer struct {
	template *template.Template
	debug    bool
	location string
	f        template.FuncMap
}

func NewRenderer(location string, debug bool, f template.FuncMap) *Renderer {
	t := new(Renderer)
	t.location = location
	t.debug = debug
	t.f = f

	t.ReloadTemplates()

	return t
}

func (t *Renderer) ReloadTemplates() {
	// funcs := template.FuncMap{"addInt": addInt}
	t.template = template.Must(template.ParseGlob(t.location))
	// .Funcs(funcs)
}
func (t *Renderer) Render(
	w io.Writer,
	name string,
	data interface{},
	c echo.Context,
) error {
	if t.debug {
		t.ReloadTemplates()
	}

	return t.template.ExecuteTemplate(w, name, data)
}
