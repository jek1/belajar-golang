package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"text/template"
	"time"
	"webecho/utils"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type M map[string]interface{}

var apiStudents = func(c echo.Context) error {
	// var err error
	var listStudent = []*Student{
		{"E001", "ethan", 21},
		{"W001", "wick", 22},
		{"B001", "bourne", 23},
		{"B002", "bond", 23},
	}

	if name := c.QueryParam("name"); name != "" {
		one := &Student{ID: "C001", Name: name}
		if sAge := c.QueryParam("age"); (sAge) != "" {
			if iAge, errAge := strconv.Atoi(sAge); errAge == nil {
				one.Age = iAge
			}
		}
		listStudent = append(listStudent, one)
	}

	data := M{
		"response_status":  true,
		"response_message": "",
		"response_code":    200,
		"list_student":     listStudent,
	}
	return c.JSON(http.StatusOK, data)
}

var getStudents = func(c echo.Context) error {
	url := "http://localhost:9200/api/students"
	urlParam := ""

	if name := c.QueryParam("name"); name != "" {
		if urlParam == "" {
			urlParam += "?"
		} else {
			urlParam += "&"
		}
		urlParam += "name=" + name
	}

	if age := c.QueryParam("age"); age != "" {
		if urlParam == "" {
			urlParam += "?"
		} else {
			urlParam += "&"
		}
		urlParam += "age=" + age
	}

	url += urlParam

	// apiResult := oneResultStudent{}
	var apiResult echo.Map
	errURL := getURLasJSON(url, &apiResult)
	if errURL != nil {
		log.Fatal(errURL)
	}

	apiResult["URL"] = url
	apiResult["method"] = c.Request().Method
	apiResult["uri"] = c.Request().RequestURI
	apiResult["title"] = "Get Students"
	apiResult["addInt"] = func(i1, i2 int) int {
		return i1 + i2
	}
	return c.Render(http.StatusOK, "get_student.html", apiResult)
}

type oneResultStudent struct {
	ResponseStatus  bool      `json:"response_status"`
	ResponseMessage string    `json:"response_message"`
	ResponseCode    int64     `json:"response_code"`
	ListStudent     []Student `json:"list_student"`
	URL             string    `json:"url"`
	Custom          map[string]interface{}
	CustomFunc      interface{}
}

func getURLasJSON(url string, result interface{}) error {
	// url := "http://api.open-notify.org/astros.json"
	httpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", "golang-tutorial")
	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	errJSON := json.Unmarshal(body, &result)
	if errJSON != nil {
		log.Fatal(errJSON)
	}

	return nil
}

type tJob struct {
	JobID     string
	JobTitle  string
	MinSalary int64
	MaxSalary int64
}

func getOracle(c echo.Context) error {
	var err error

	db := utils.OpenConnectionDB()
	defer db.Close()

	sql := "SELECT JOB_ID, JOB_TITLE, MIN_SALARY, MAX_SALARY FROM HR.JOBS"
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println(err)
		return err
	}

	var listJob []tJob

	defer rows.Close()
	for rows.Next() {
		var JobID string
		var JobTitle string
		var MinSalary int64
		var MaxSalary int64
		rows.Scan(&JobID, &JobTitle, &MinSalary, &MaxSalary)

		var one = tJob{JobID, JobTitle, MinSalary, MaxSalary}
		listJob = append(listJob, one)
	}

	fmt.Println("listJob: ", listJob)

	data := M{
		"response_status":  true,
		"response_message": "",
		"response_code":    200,
		"list_job":         listJob,
	}
	return c.JSON(http.StatusOK, data)
}

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "/home/jeki/work/go/src/webecho/static",
		Browse: false,
	}))

	var addInt = func(i, i2 int) int {
		return i + i2
	}
	funcs := template.FuncMap{"addInt": addInt}
	e.Renderer = NewRenderer("/home/jeki/work/go/src/webecho/templates/*.html", true, funcs)

	e.GET("/", func(ctx echo.Context) error {
		data := "Hello from /index"
		return ctx.String(http.StatusOK, data)
	})

	e.GET("/json", func(ctx echo.Context) error {
		listStudents := []Student{
			{"e001", "Jason", 23},
			{"e002", "Merry", 21},
		}
		data := M{
			"response_status":  true,
			"response_message": "This is the Json Results",
			"students":         listStudents,
		}
		return ctx.JSON(http.StatusOK, data)
	})

	e.GET("/html", func(ctx echo.Context) error {
		data := "Hello from /html"
		return ctx.HTML(http.StatusOK, data)
	})

	e.GET("/api/students", apiStudents)
	e.GET("/get/students", getStudents)
	e.GET("/get/oracle", getOracle)
	e.Start(":9200")
}
