package main

type Student struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

var students = []*Student{}

func init() {
	students = append(students, &Student{"A1", "Andi", 10})
	students = append(students, &Student{"A2", "Budi", 10})
	students = append(students, &Student{"A3", "Charlie", 10})
	students = append(students, &Student{"A4", "Dedi", 10})
	students = append(students, &Student{"A5", "Elie", 10})
}
