package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

var baseURL = "http://localhost:9200"

type Student struct {
	ID    string
	Name  string
	Grade int
}

type oneResultStudent struct {
	ResponseStatus  bool      `json:"response_status"`
	ResponseMessage string    `json:"response_message"`
	ResponseCode    int64     `json:"response_code"`
	ListStudent     []Student `json:"list_student"`
	HTTPMethod      string    `json:"http_method"`
	URLAPI          string    `json:"url_api"`
}

func addInt(x, y int) int {
	return x + y
}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "apa kabar!")
}

func cekurl(w http.ResponseWriter, r *http.Request) {
	// var urlString = "http://kalipare.com:80/hello?name=john wick&age=27"
	var urlString = fmt.Sprintf("%s%s", r.Host, r.URL.Path)

	// var u, e = url.Parse(urlString)
	// if e != nil {
	// 	fmt.Println(e.Error())
	// 	return
	// }

	var u = r.URL

	var Name string = "not set"
	value, isset := u.Query()["name"]
	if isset {
		Name = value[0]
	}

	var Age string = ""
	valueAge, issetAge := u.Query()["age"]
	if issetAge {
		Age = valueAge[0]
	}

	var data = map[string]string{
		"Url":      urlString,
		"Protocol": u.Scheme,
		"Rhost":    r.Host,
		"Path":     u.Path,
		"Name":     Name, // john wick
		"Age":      Age,
	}

	var t, err = template.ParseFiles("src/web1/views/template.cekurl.html")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	t.Execute(w, data)
}

func tmpl(w http.ResponseWriter, r *http.Request) {
	var name string = "Jokowe"
	var data = map[string]string{
		"Name":    name,
		"Message": "Piye kabare Mas " + name,
	}

	var t, err = template.ParseFiles("src/web1/views/template.html")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	t.Execute(w, data)
}

func tmpl2(w http.ResponseWriter, r *http.Request) {
	viewName := "src/web1/views/template.html"
	funcs := template.FuncMap{"addInt": addInt}
	t := template.Must(template.New("template.html").Funcs(funcs).ParseFiles(viewName))

	var name string
	name = "Jokowe 22222"
	var data = map[string]string{
		"Name":    name,
		"Message": "Piye kabare Mas " + name,
	}
	t.Execute(w, data)
}

func apiStudent(w http.ResponseWriter, r *http.Request) {
	var err error

	var urlAPI string = r.Host + r.URL.Path

	w.Header().Set("Content-Type", "application/json")
	var listStudent = []Student{
		{"E001", "ethan", 21},
		{"W001", "wick", 22},
		{"B001", "bourne", 23},
		{"B002", "bond", 23},
	}

	var u = r.URL

	var Name string
	valName, isName := u.Query()["name"]
	if isName {
		Name = valName[0]
	}

	var Grade int
	valGrade, isGrade := u.Query()["grade"]
	if isGrade {
		Grade, err = strconv.Atoi(valGrade[0])
		if err != nil {
			fmt.Println("error: ", err.Error())
		}
	}

	// if isName && isGrade {
	if isName {
		one := Student{"New GET", Name, Grade}
		listStudent = append(listStudent, one)
	}

	r.ParseForm()
	var postName = r.FormValue("post_name")
	if postName != "" {
		one := Student{"New POST", postName, 1000}
		listStudent = append(listStudent, one)
	}

	var responseMessage string
	if r.Method == "GET" {
		responseMessage = "GET | param Name = " + Name
	} else if r.Method == "POST" {
		responseMessage = "POST | param Name = " + Name
	} else {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	var data map[string]interface{}
	data = map[string]interface{}{
		"response_code":    200,
		"response_status":  true,
		"response_message": responseMessage,
		"list_student":     listStudent,
		"http_method":      r.Method,
		"url_api":          urlAPI,
	}

	fmt.Println("map data: ", data)
	var result []byte
	result, err = json.Marshal(data)
	// fmt.Println("map marshaled: ", result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(result)
	return
}

func getPeople(w http.ResponseWriter, r *http.Request) {
	// url := baseURL + "/api/student"
	type onePeople struct {
		Name  string `json:"name"`
		Craft string `json:"craft"`
	}
	type oneResultPeople struct {
		Message string      `json:"message"`
		Number  int         `json:"number"`
		People  []onePeople `json:"people"`
	}

	url := "http://api.open-notify.org/astros.json"
	httpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", "golang-tutorial")
	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	apiRes := oneResultPeople{}
	jsonErr := json.Unmarshal(body, &apiRes)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	fmt.Fprintln(w, "Message: ", apiRes.Message)
	fmt.Fprintln(w, "Number: ", apiRes.Number)
	// fmt.Fprintln(w, "People: ", apiRes.People)

	x := 0
	for _, one := range apiRes.People {
		x++
		fmt.Fprintln(w, "index: ", x, " | Name: ", one.Name, " | Craft: ", one.Craft)
	}
}

func getStudent(w http.ResponseWriter, r *http.Request) {
	var err error

	urlAPIStudent := baseURL + "/api/student"
	httpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	var u = r.URL

	var getName string
	valName, isName := u.Query()["name"]
	if isName {
		getName = valName[0]
	} else {
		getName = "Nama Default untuk GET"
	}
	var req *http.Request

	var postName = "NAMA DEFAULT UNTUK POST"
	fmt.Println("INI ISI POSTNAME 1: ", postName)
	if r.Method == "GET" {
		fmt.Println("method GET")
	} else if r.Method == "POST" {
		fmt.Println("method POST")

		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// postName = r.FormValue("post_name")
		postName = r.Form.Get("post_name")
		fmt.Println("INI ISI POSTNAME 2: ", postName)
		if postName == "" {
			postName = "NAMA BARU METHOD POST"
			fmt.Println("INI ISI POSTNAME 3: ", postName)
		}

		//INI UNTUK POST Method
		var param = url.Values{}
		param.Set("post_name", postName)
		var payload = bytes.NewBufferString(param.Encode())
		req, err = http.NewRequest(http.MethodPost, urlAPIStudent, payload)
		// req, err := http.NewRequest(http.MethodGet, urlAPIStudent, nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	} else {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	q := req.URL.Query()
	q.Add("name", getName)
	req.URL.RawQuery = q.Encode()

	req.Header.Set("User-Agent", "golang-tutorial")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	apiRes := oneResultStudent{}
	jsonErr := json.Unmarshal(body, &apiRes)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	// fmt.Fprintln(w, "Response Code: ", apiRes.ResponseCode)
	// fmt.Fprintln(w, "Response Message: ", apiRes.ResponseMessage)
	// fmt.Fprintln(w, "Response Status: ", apiRes.ResponseStatus)

	// x := 0
	// for _, one := range apiRes.ListStudent {
	// 	x++
	// 	fmt.Fprintln(w, "index: ", x, " | Name: ", one.Name, " | Grade: ", one.Grade)
	// }

	// t, err := template.ParseFiles("template.get.student.html")
	// t, err := template.ParseFiles("src/web1/views/template.student.html")
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	return
	// }
	viewName := "src/web1/views/template.student.html"
	funcs := template.FuncMap{"addInt": addInt}
	t := template.Must(template.New("template.student.html").Funcs(funcs).ParseFiles(viewName))

	data := map[string]interface{}{
		"ListStudent":     apiRes.ListStudent,
		"Title":           "List Student From API",
		"ResponseMessage": apiRes.ResponseMessage,
		"UrlAPIStudent":   urlAPIStudent,
		"HTTPMethod":      apiRes.HTTPMethod,
		"URLAPI":          apiRes.URLAPI,
		"PostName":        postName,
	}
	t.Execute(w, data)
}

func main() {
	http.Handle("/static/",
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("src/web1/assets"))))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "halo!")
	})

	http.HandleFunc("/index", index)
	http.HandleFunc("/tmpl", tmpl)
	http.HandleFunc("/tmpl2", tmpl2)
	http.HandleFunc("/cekurl", cekurl)
	http.HandleFunc("/api/student", apiStudent)
	http.HandleFunc("/get/student", getStudent)

	fmt.Println("starting web server at http://localhost:9200/")
	http.ListenAndServe(":9200", nil)
}
